﻿const constant = 1.045; //1.040
const topRank = 2029; //1970 actual
let arrCategory = [
  "👑 Leyenda",
  "🥇 Gran Estrella",
  "🥇 Estrella",
  "🥇 Héroe",
  "🥇 Maestro",
  "🥈 Maravilloso",
  "🥈 Increíble",
  "🥈 Brillante",
  "🥈 Renombrado",
  "🥈 Famoso",
  "🥉 Talentoso",
  "🥉 Popular",
  "🥉 Competente",
  "🥉 Ambicioso",
  "🥉 Promesa",
  "Esperanzador",
  "Avanzado",
  "Habilidoso",
  "Principiante",
  "🔰 Novato"
];
let counter = 0
let categoria = {}
function asign(number) {
  if (counter == arrCategory.length)
    return;
  categoria[number] = arrCategory[counter]
  counter++;
  asign(Math.round(number / constant))
}

asign(topRank)

export { categoria };

/* export var categoria = {
  2029: "👑 Leyenda",
  1942: "🥇 Gran estrella",
  1858: "🥇 Estrella",
  1778: "🥇 Héroe",
  1701: "🥇 Maestro",
  1628: "🥈 Maravilloso",
  1558: "🥈 Increíble",
  1491: "🥈 Brillante",
  1427: "🥈 Renombrado",
  1366: "🥈 Famoso",
  1307: "🥉 Talentoso",
  1251: "🥉 Popular",
  1197: "🥉 Competente",
  1145: "🥉 Ambicioso",
  1096: "🥉 Promesa",
  1049: "Esperanzador",
  1004: "Avanzado",
  961: "Habilidoso",
  920: "Principiante",
  0: "🔰 Novato"
} */