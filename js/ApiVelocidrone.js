﻿import { categoria } from './data/category.js'
// PARSE TO JSON
export function parseTABLEtoJSON(table) {
  if (table) {
    let tableHeader = Array.from(table.children[0].children[0].getElementsByTagName('th'))
    let tableBody = Array.from(table.children[1].getElementsByTagName('tr'))
    let banderas = document.createElement("span")
    var toJSON = []

    for (let node = 0; node < tableBody.length; node++) {
      let dict = {};
      for (let data = 0; data < tableBody[node].children.length; data++) {
        dict[tableHeader[data].innerHTML] = !isNaN(tableBody[node].children[data].innerHTML.trim()) ? parseFloat(tableBody[node].children[data].innerHTML.trim()) : tableBody[node].children[data].innerHTML.trim()
      }

      // COUNTRY
      banderas.innerHTML = dict.Country
      if (banderas.firstChild.outerHTML)
        dict.Country = { icon: banderas.firstChild.className, text: banderas.textContent.trim() }

      // CATEGORY
      dict.Category = function () {
        let aux = [];
        Object.keys(categoria).map((position) => {
          position = parseInt(position)
          if (dict.hasOwnProperty("Ranking")) {
            if (position <= dict.Ranking) {
              aux.push(position);
            }
          } else {
            if (position <= dict.Rank) {
              aux.push(position);
            }
          }
        })
        return (parseInt(Object.keys(categoria)[0]) >= dict.Ranking) ? categoria[parseInt(Object.keys(categoria)[0])] : categoria[Math.max(...aux)] != undefined ? categoria[Math.max(...aux)] : categoria[Object.keys(categoria)[0]]
      }
      toJSON.push(dict)
    }
    return toJSON
  }
  else {
    return false
  }
}

export function showTable(api) {
  // TABLE HEADER
  document.getElementById('tablaHeader').innerHTML = ""
  let aux = "";
  let table = Object.keys(api[0]);
  for (let title of table)
    aux += `<th>${title}</th>`
  document.getElementById('tablaHeader').innerHTML += aux;

  // TABLE BODY
  document.getElementById('tablaBody').innerHTML = ""
  for (let index = 0; index < api.length; index++) {
    let aux2 = "";
    for (let data of Object.values(api[index]))
      aux2 += `<td ${(typeof (data) == 'function') ? `class='label-rounded' data-content=${data().replace(/ /g, "")}` : ""}>${(typeof (data) == 'function') ? data() : (typeof (data) == 'object') ? `<span class="${data.icon}"></span> ${data.text}` : data}</td>`;
    document.getElementById('tablaBody').innerHTML += aux2;
  }
}