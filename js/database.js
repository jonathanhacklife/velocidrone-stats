﻿// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
var firebaseConfig = {
  apiKey: "AIzaSyD5Pg3Z6lk1o7vviBfWFV68bqyBT4MQIGw",
  authDomain: "velocidronestats.firebaseapp.com",
  projectId: "velocidronestats",
  storageBucket: "velocidronestats.appspot.com",
  messagingSenderId: "1015345946745",
  appId: "1:1015345946745:web:6ec6fe2f7a93a783d634a8",
  measurementId: "G-0J4LPZK1PF"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
firebase.analytics();

// Get a reference to the database service
export var database = firebase.database();