﻿// https://www.chartjs.org/docs/latest/getting-started/usage.html
import { categoria } from './data/category.js'

function dynamicColors() {
  var r = Math.floor(Math.random() * 255);
  var g = Math.floor(Math.random() * 255);
  var b = Math.floor(Math.random() * 255);
  return "rgba(" + r + "," + g + "," + b + ", 1)";
}

export function grafico(id, type, valorX, valorY, leyenda) {
  var myChart = new Chart(document.getElementById(id).getContext('2d'), {
    type: type,
    data: {
      labels: valorX, // VALORES (X)
      datasets: [{
        label: leyenda, // LEYENDA
        data: valorY, // VALORES (Y)
        backgroundColor: [],
        borderWidth: 5
      }]
    },
    options: {
      responsive: true,
      scales: {
        y: {
          beginAtZero: true
        }
      },
    }
  });
  var dataset = myChart.data.datasets[0];
  for (var i = 0; i < dataset.data.length; i++) {
    if (parseInt(dataset.data[i]) >= Object.keys(categoria)[15]) {
      dataset.backgroundColor[i] = "gold";
    } else if (parseInt(dataset.data[i]) >= Object.keys(categoria)[10]) {
      dataset.backgroundColor[i] = "grey";
    } else if (parseInt(dataset.data[i]) >= Object.keys(categoria)[5]) {
      dataset.backgroundColor[i] = "orange";
    } else if (parseInt(dataset.data[i]) < Object.keys(categoria)[5]) {
      dataset.backgroundColor[i] = "white";
    }
  }
  myChart.update();
}

export function graphMap(id, dataCountry, background_color, axisColor) {
  google.charts.load('current', { 'packages': ['geochart'] });
  google.charts.setOnLoadCallback(drawRegionsMap);

  function drawRegionsMap() {
    let paises = Object.entries(dataCountry)
    paises.unshift(['Country', 'Popularidad'])
    var data = google.visualization.arrayToDataTable(paises);

    var options = {
      colorAxis: { colors: axisColor },
      datalessRegionColor: '#444',
      backgroundColor: background_color,
    };

    var chart = new google.visualization.GeoChart(document.getElementById(id));

    chart.draw(data, options);
  }
}