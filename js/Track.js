﻿import { showTable, parseTABLEtoJSON } from './ApiVelocidrone.js';
import { grafico, graphMap } from './VelocidroneChart.js';
import { trackList } from './data/tracksList.js'
import {categoria} from './data/category.js'
/* import { tablaTrack as tabla } from './data/fakeData.js'; */

export function Track(track, player) {

   // VERIFICACIÓN NOMBRE DE USUARIO
   if (!player || player == "null") {
      window.open(`./404.html`, "_self")
      return "No se encuentra el usuario"
   }
   // CARGAR PISTAS POR PRIMERA VEZ
   if (document.getElementById("tracks").children.length == 0) {
      console.log("Cargando escenarios...")
      for (let pista of trackList) {
         document.getElementById("tracks").innerHTML += `<option value='${pista.trackName}'>${pista.trackName}</option>`
      }
   }
   // CARGAR LOCALSTORAGE
   if (localStorage.getItem("trackName") != null)
      document.getElementById("tracks").value = JSON.parse(localStorage.getItem("trackName"))

   // BUTTON GROUP
   document.getElementById('chooseTrack').innerHTML = `
         <a href="./hangar.html?${player}" class="btn btn-warning">Volver al hangar</a>
         <button id="searchTrack" class="btn btn-warning">Buscar mapa</button>
      `

   // CARGAR TRACK
   document.getElementById("searchTrack").onclick = function () {
      localStorage.setItem("trackLink", JSON.stringify(trackList[document.getElementById("tracks").selectedIndex].link));
      localStorage.setItem("trackName", JSON.stringify(trackList[document.getElementById("tracks").selectedIndex].trackName));
      Track(trackList[document.getElementById("tracks").selectedIndex].link, player)
   }

   document.getElementById("dataMiddle").innerHTML = `
         <!-- GRÁFICOS -->
         <div class="col-md-6">
            <canvas id="chartAverage"></canvas>
         </div>
         <div class="col-md-6">
            <div id="regions_div" class="col"></div>
         </div>
         <!-- <canvas id="chartCountry"></canvas> -->
      `

   // HACK: EVITAR UTILIZAR LA API SI YA SE USÓ UNA VEZ POR SESIÓN
   console.log("Cargando datos del escenario...")
   fetch('https://api.webscrapingapi.com/v1?api_key=Zs1x5cg8GUC6sB0TN34Fn3hs6HmLDWFw&url=' + track).then(srs => srs.text()).then(data => {
      console.log("Listo ✔")
      console.timeEnd("Tiempo de ejecución de carga de API")
      var html = document.createElement('html');
      html.innerHTML = data
      let tabla = html.getElementsByTagName('table')[0]

      // TITULO
      window.document.title = `${JSON.parse(localStorage.getItem("trackName"))}`
      document.getElementById("title").innerHTML = `${JSON.parse(localStorage.getItem("trackName"))}`

      // API
      var apiTrack = parseTABLEtoJSON(tabla)
      let mainPlayer = apiTrack.filter(user => user.Player == player)[0];

      const listBy = (tag) => apiTrack.map(item => item[tag])

      const getCountOf = (list) => list.reduce((a, b) => ({ ...a, [b]: (a[b] || 0) + 1 }), {})

      const getPopular = (list) => {
         let allCount = getCountOf(list)
         let keyDict = `${Object.keys(allCount).filter(drone => allCount[drone] >= Math.max.apply(null, Object.values(allCount)))}`
         let simpleDict = {}
         simpleDict[keyDict] = allCount[keyDict]
         return simpleDict
      }

      const getAverageByTime = (tiempo, rango) => {
         let aux = []
         let result = tiempo[0];
         let dict_TimeCount = {}

         for (let i = 0; i < rango; i++) { // DEL 0 AL rango
            result += ((tiempo[tiempo.length - 1] - tiempo[0]) / rango) // RESULTADO + 4.316
            aux.push(result.toFixed(3)) // LO AGREGO A LA LISTA
            dict_TimeCount[(aux[i])] = tiempo.filter(time => (time < aux[i] && time >= (i > 0 ? aux[i - 1] : tiempo[0]))).length
         }
         return dict_TimeCount
      }

      const getDifficultTrack = () => {
         let templateDifficult = {
            0: "Fácil",
            1: "Medio",
            2: "Difícil"
         }
         let difficult = Object.values(getAverageByTime(listBy("Time"), 3))
         return templateDifficult[difficult.indexOf(Math.max.apply(null, difficult))]
      }

      document.getElementById("difficult").innerHTML = `Dificultad: ${getDifficultTrack()}`

      if (mainPlayer) {
         const getFrontBackPlayers = (range) => apiTrack.filter(user => (user["#"] > mainPlayer["#"] - range && user["#"] < mainPlayer["#"] + range))

         const getProgress = (actual) => {
            let min = parseInt(Object.keys(categoria).filter(rank => actual > rank).pop())
            let max = parseInt(Object.keys(categoria).filter(rank => actual < rank)[0])
            let nextCategory = categoria[parseInt(Object.keys(categoria).filter(rank => actual < rank)[0])]
            return `
               <div class="progress">
                  <div class="progress-bar progress-bar-striped progress-bar-animated bg-warning text-body" role="progressbar" style="width: ${((100 / (max - min)) * (actual - min)).toFixed(1)}%" aria-valuenow="${actual}" aria-valuemin="0" aria-valuemax="100">${nextCategory}</div>-<div class="text-body">${(max - min) - (actual - min)}pts</div>
               </div>
            `
         }
         // STATS CARDS
         // document.getElementById("country").innerHTML = `${Object.keys(getPopular(apiTrack.map(item => item.Country.text)))} (${Object.values(getPopular(apiTrack.map(item => item.Country.text)))}/${apiTrack.length})`

         let cards = {
            Posición: mainPlayer["#"],
            Tiempo: mainPlayer.Time,
            Dron: mainPlayer.Model,
            Categoría: mainPlayer.Category()
         }

         document.getElementById("dataTop").innerHTML = ""
         document.getElementById("dataTop").style.display = "flex"
         Object.keys(cards).map((key) => document.getElementById("dataTop").innerHTML += `
            <div class="col-md p-2">
               <div class="card bg-secondary h-100 col">
               <h5 class="text-center card-header card-subtitle">${key}:</h5>
               <div class="card-body d-flex flex-column justify-content-center">
                  <h3 class="text-center">${cards[key]}</h3>
               </div>
               </div>
            </div>
         `)

         // DATA MIDDLE
         document.getElementById("dataMiddle").innerHTML = `
         <!-- GRÁFICOS -->
         <div class="col-md-8">
            <canvas id="chartAverage"></canvas>
            <div id="regions_div" class="col"></div>
            <!-- <canvas id="chartCountry"></canvas> -->
         </div>

         <!-- DATOS EN FRENTE - DETRÁS -->
         <div id="dataFrontBack" class="col">
            <h5 class="text-center">En frente</h5>
            <div id="players" class="col"></div>
            <h5 class="text-center">Detrás</h5>
         </div>
         `

         // PANEL EN FRENTE - DETRAS
         for (let pilot of getFrontBackPlayers(3)) {
            // FEATURE: Agregar barra de progreso: https://getbootstrap.com/docs/5.0/components/progress/
            document.getElementById("players").innerHTML += `
            <div class="card bg-secondary col m-2">
               <a class="text-light text-decoration-none" href="./hangar.html?${pilot.Player}">
                  <div class="card-header">
                     <div class="row">
                        <label class="col card-text text-center"><strong>#${pilot["#"]}</strong></label>
                        <label class="col card-text text-center">${pilot.Time}s</label>
                        <label class="col card-text text-center">${'+'.repeat((mainPlayer.Time - pilot.Time) > 0) + (mainPlayer.Time - pilot.Time).toFixed(3)}s</label>
                     </div>
                  </div>
                  <div class="card-body py-0">
                     <div class="row">
                        <img class="col text-center ${pilot.Country.icon}" src="http://pngimg.com/uploads/drone/drone_PNG33.png" alt="drone">
                        <div class="col-md-6">
                           <h4 class="card-title">${pilot.Player}</h4>
                           <h6>${pilot.Country.text}</h6>
                           <h6>${pilot.Model}</h6>
                           <h6>Próxima categoría:</h6>
                           <div class="pb-3">${getProgress(pilot.Ranking)}</div>
                        </div>
                     </div>
                  </div>
                  <div class="card-footer">
                     <div class="row">
                        <label class="col card-text text-center">⭐${pilot.Ranking}</label>
                        <label class="col card-text text-center">${pilot.Category()}</label>
                     </div>
                  </div>
               </a>
            </div>
            `
         }
      } else {
         document.getElementById("dataTop").style.display = "none"
      }

      // GRÁFICOS
      grafico("chartAverage", "bar", Object.keys(getAverageByTime(listBy("Time"), 20)), Object.values(getAverageByTime(listBy("Time"), 20)), "Cantidad de pilotos")
      graphMap("regions_div", getCountOf(apiTrack.map(item => item.Country.text).sort()), "transparent", ["green", "lime",])

      // grafico("chartCountry", "bar", Object.keys(getCount(apiTrack.map(item => item.Country.text).sort())), Object.values(getCount(apiTrack.map(item => item.Country.text).sort())), "Cantidad de pilotos")

      showTable(apiTrack)

   })
}
let usuario = window.location.search.slice(1, window.location.search.length).replace('%20', " ")
Track((JSON.parse(localStorage.getItem("trackLink")) == null) ? 'https://velocidrone.com/leaderboard/30/672/All' : JSON.parse(localStorage.getItem("trackLink")), usuario)