﻿import { showTable, parseTABLEtoJSON } from './ApiVelocidrone.js';
import { grafico } from './VelocidroneChart.js';
import { categoria } from './data/category.js'
/* import { tablaUser as tabla } from './data/fakeData.js'; */
export function User(name) {

   if (!name || name == "null") {
      window.open(`./404.html`, "_self")
      return "No se encuentra el usuario"
   }

   var history = 'https://www.velocidrone.com/ranking/get_history/' + name

   // HACK: EVITAR UTILIZAR LA API SI YA SE USÓ UNA VEZ POR SESIÓN
   console.log("Cargando datos del usuario...")
   console.time("Tiempo de ejecución de carga de API")
   fetch('https://api.webscrapingapi.com/v1?api_key=Zs1x5cg8GUC6sB0TN34Fn3hs6HmLDWFw&url=' + history).then(srs => srs.text()).then(data => {
      console.log("Listo ✔")
      console.timeEnd("Tiempo de ejecución de carga de API")
      var html = document.createElement('html')
      html.innerHTML = data
      let table = html.getElementsByTagName('table')[0]
      var apiUser = parseTABLEtoJSON(table)
      /* console.table(apiUser) */
      // OBTIENE LA TABLA
      showData(apiUser, name)
   });
}

function showData(apiUser, name) {

   // SI LA TABLA DEL USUARIO EXISTE, LA MUESTRA EN PANTALLA
   if (apiUser) {
      window.document.title = `Velocidrone Stats - ${name}`
      document.getElementById("player").innerHTML = `${name} <button id="choosePilot" class="btn btn-light"><i class="fas fa-pen"></i></button>`
      document.getElementById("dataPlayer").innerHTML = `
         <p>Última conexión: <span id="last-connection">${apiUser[0].Date} (hace ${Math.round((new Date().getTime() - new Date(apiUser[0].Date).getTime()) / (1000 * 60 * 60 * 24))} días)</span></p>
         <a id="chooseTrack" class="btn btn-info" href="./Track.html?${name}">Ir a la pista</a>
         <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">Ver categorías</button>`

      /* INGRESAR NOMBRE DE PILOTO */
      document.getElementById('choosePilot').onclick = function () {
         name = prompt('Ingrese el nombre del piloto')
         window.open(`./hangar.html?${name}`, "_self")
      };

      // FEATURE: Obtener ubicación desde Firebase

      // LAST CONNECTION
      // FIXME: Obtener fecha en horas y minutos y convertir a Date()

      // TOP PANEL

      const getProgress = (actual) => {
         let min = parseInt(Object.keys(categoria).filter(rank => actual > rank).pop())
         let max = parseInt(Object.keys(categoria).filter(rank => actual < rank)[0])
         let nextCategory = categoria[parseInt(Object.keys(categoria).filter(rank => actual < rank)[0])]
         return `
            <div class="progress">
               <div class="progress-bar progress-bar-striped progress-bar-animated bg-warning text-body" role="progressbar" style="width: ${((100 / (max - min)) * (actual - min)).toFixed(1)}%" aria-valuenow="${actual}" aria-valuemin="0" aria-valuemax="100">${nextCategory}</div>-<div class="text-body">${(max - min) - (actual - min)}pts</div>
            </div>
            `
      }

      let cards = {
         "Posición global": `${apiUser[0].Rank} ${(apiUser[0].Rank - apiUser[1].Rank) > 0 ? `<span class="badge bg-warning">+${apiUser[0].Rank - apiUser[1].Rank}</span>` : `<span class="badge bg-danger">${apiUser[0].Rank - apiUser[1].Rank}</span>`}`,
         "Categoría": apiUser[0].Category(),
         "Próxima categoría": `<div class="p-2">${getProgress(apiUser[0].Rank)}</div>`
      }
      Object.keys(cards).map((key) => document.getElementById("topPanel").innerHTML += `
               <div class="col">
                  <div class="card bg-secondary m-2">
                     <div class="card-header">
                        <h6 class="text-center">${key}:</h6>
                     </div>
                     <div class="card-body">
                        <h3 id="global-position" class="text-center">${cards[key]}</h3>
                     </div>
                  </div>
               </div>
               `)

      grafico("Categorias", "horizontalBar", Object.values(categoria).reverse(), Object.keys(categoria).reverse(), "Tabla de puntuación")

      let rankHistoryY = [];
      let rankHistoryX = [];
      apiUser.map(item => {
         rankHistoryY.push(item.Rank)
         rankHistoryX.push(item.Category())
      })
      grafico("historyChart", "bar", rankHistoryX.reverse(), rankHistoryY.reverse(), "Ranking online")

      // TITLE TABLE
      document.getElementById("titleTable").innerHTML = `Historial online`
      showTable(apiUser)
   }
   else {
      window.open(`./404.html`, "_self")
      return "No se encuentra el usuario"
   }
}

let usuario = window.location.search.slice(1, window.location.search.length).replace('%20', " ")
User(usuario)